import com.devcamp.bookauthor.models.Author;
import com.devcamp.bookauthor.models.Book;

public class BookAuthorClass {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Truong Tan Loc", "locttce170037@fpt.edu.vn", 'm');
        System.out.println("Author 1: ");
        System.out.println(author1);

        System.out.println("----------------------------------------------------");

        Author author2 = new Author("vuong Thi My Cam", "Camvtmcs160075@fpt.edu.vn", 'f');
        System.out.println("Author 1: ");
        System.out.println(author2);
        System.out.println("----------------------------------------------------");


        Book book1 = new Book("java", author1, 10000);
        System.out.println("Book 1: ");
        System.out.println(book1);

        System.out.println("----------------------------------------------------");

        
        Book book2 = new Book("Reactjs", author2, 30000, 100);
        System.out.println("Book 2: ");
        System.out.println(book2);

    }
}

