package com.devcamp.bookauthor.models;

public class Book {
     private String name;
     private Author author;
     private double price;
     private int qty = 0;

     


     public Book() {
          
     }


     public Book(String name, Author author, double price) {
          this.name = name;
          this.author = author;
          this.price = price;
     }


     public Book(String name, Author author, double price, int qty) {
          this.name = name;
          this.author = author;
          this.price = price;
          this.qty = qty;
     }


     public String getName() {
          return name;
     }


     public double getPrice() {
          return price;
     }


     public void setPrice(double price) {
          this.price = price;
     }


     public int getQty() {
          return qty;
     }


     public void setQty(int qty) {
          this.qty = qty;
     }


     public Author getAuthor() {
          return author;
     }

     @Override
     public String toString(){
          //return "Book[name = "+ this.name +  ", Author[name = " + this.author.getName() + ", email = " + this.author.getEmail() + ", gender = " + this.author.getGender() + "], price = " + this.price + ",qty = " + this.qty + "]";
          return "Book[name = "+ this.name +  ", "+ this.author + ", price = " + this.price + ",qty = " + this.qty + "]";
     }

     
     
}
